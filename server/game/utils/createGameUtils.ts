import {
  HiddenCard,
  HiddenInfluence,
  NotVisible,
  PlayerID,
  PlayerState,
} from "../types/gameTypes";
import { SelectAction } from "../actions/SelectAction";

export function shuffleArray<T>(input: Array<T>): Array<T> {
  let array = [...input];
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
}

function createDeck(numberEach: 3 | 4 | 5): Array<HiddenCard> {
  const cards: Array<HiddenCard> = [
    "DUKE",
    "ASSASSIN",
    "CONTESSA",
    "CAPTAIN",
    "AMBASSADOR",
  ];
  return shuffleArray(
    cards.flatMap((card) => [...Array(numberEach)].map((_) => card)),
  );
}

function getNumberOfEachCard(playerCount: number): 3 | 4 | 5 {
  if (playerCount <= 6) {
    return 3;
  }
  if (playerCount <= 8) {
    return 4;
  }
  return 5;
}

export function createGame(players: Array<PlayerID>): SelectAction {
  const deck = createDeck(getNumberOfEachCard(players.length));
  const playerHands: Array<PlayerState<HiddenInfluence>> = players.map(
    (playerID) => ({
      influence: [deck.shift(), deck.shift()].flatMap((card) =>
        card != null
          ? [
              {
                card,
                visibility: {
                  type: "visible_to_player",
                  visibleToPlayerId: playerID,
                },
              },
            ]
          : [],
      ),
      coins: 2,
      id: playerID,
    }),
  );
  const influenceDeck = deck.map((card) => {
    const visibility: NotVisible = { type: "not_visible" };
    return { card, visibility };
  });
  return SelectAction.create({
    gameState: {
      players: playerHands,
      deck: influenceDeck,
      currentPlayerId: players[0],
      revealedCards: [],
    },
  });
}
