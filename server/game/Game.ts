import { ClientGameState, PlayerID } from "./types/gameTypes";
import { createGame } from "./utils/createGameUtils";
import { ActionState } from "./actions/base/ActionState";
import { GameEventLogger, ReceivedGameEvent } from "./types/gameEvents";
import assert from "assert";
import { convertState } from "./convertToClient";
import { PlayerAction } from "./types/PlayerAction";
import { jsonMember, jsonObject, toJson } from "typedjson";
import { BaseActionState } from "./actions/base/BaseActionState";

@jsonObject
@toJson
export class Game {
  @jsonMember(BaseActionState)
  state: ActionState;

  protected constructor(state: ActionState | undefined) {
    this.state = state ?? createGame([]);
  }
  get playerIDs(): Array<PlayerID> {
    return this.state.state.gameState.players.map(({ id }) => id);
  }
  get gameOver(): boolean {
    return (
      this.state.state.gameState.players.filter(
        (player) => player.influence.length > 0,
      ).length <= 1
    );
  }
  onEvent(event: ReceivedGameEvent, logger: GameEventLogger): boolean {
    try {
      this.state = this.state.nextState(event, logger);
    } catch (e) {
      console.log(e);
      return false;
    }
    return true;
  }
  getPlayerAction(playerId: PlayerID): PlayerAction {
    if (this.gameOver) {
      return { type: "GAME_OVER" };
    }
    const action = this.state.getPlayerActions().get(playerId);
    assert(action !== undefined);
    return action;
  }
  getGameStateForPlayer(playerId: PlayerID): ClientGameState {
    return {
      gameState: convertState(this.state.state.gameState, playerId),
      playerAction: this.getPlayerAction(playerId),
    };
  }

  static createGame(players: Array<PlayerID>): Game {
    return new Game(createGame(players));
  }
}
