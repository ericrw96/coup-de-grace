import {
  HiddenInfluence,
  PlayerID,
  PlayerState,
  ServerGameState,
} from "../types/gameTypes";
import assert from "assert";

export function getPlayer(
  state: ServerGameState,
  player: PlayerID
): PlayerState<HiddenInfluence> {
  const found_player = state.gameState.players.find(({ id }) => id === player);
  assert(found_player !== undefined);
  return found_player;
}

export function isDead(player: PlayerState<HiddenInfluence>): boolean {
  return player.influence.length === 0;
}

export function isPlayerDead(
  state: ServerGameState,
  player: PlayerID
): boolean {
  return isDead(getPlayer(state, player));
}
