import {
  HiddenInfluence,
  PlayerID,
  PlayerState,
  ServerGameState,
} from "../types/gameTypes";
import assert from "assert";
import { isPlayerDead } from "./StateUtils";

export function changePlayerMoney(
  state: ServerGameState,
  player: PlayerID,
  change: number,
  allowLess: boolean = false
): [ServerGameState, number] {
  let amount_changed = Math.abs(change);
  const newPlayerArray: Array<PlayerState<HiddenInfluence>> = [];
  for (const { id, coins, ...rest } of state.gameState.players) {
    if (id === player) {
      const newAmount = coins + change;
      if (newAmount < 0) {
        assert(!allowLess);
        amount_changed = Math.abs(change) - Math.abs(newAmount);
      }
      newPlayerArray.push({ id, coins: Math.max(newAmount, 0), ...rest });
    } else {
      newPlayerArray.push({ id, coins, ...rest });
    }
  }
  return [
    { ...state, gameState: { ...state.gameState, players: newPlayerArray } },
    amount_changed,
  ];
}

function nextPlayer(state: ServerGameState, currentPlayer: PlayerID): PlayerID {
  const players = state.gameState.players;
  const playerIndex = players.findIndex(({ id }) => id === currentPlayer);
  const nextPlayerIndex = (playerIndex + 1) % players.length;
  const nextPlayerID = players[nextPlayerIndex].id;
  if (isPlayerDead(state, nextPlayerID)) {
    return nextPlayer(state, nextPlayerID);
  }
  return nextPlayerID;
}
export function goToNextPlayer(state: ServerGameState): ServerGameState {
  const nextPlayerID = nextPlayer(state, state.gameState.currentPlayerId);
  // this will also remove all optional state only kept for specific actions.
  // this is intentional to only keep that optional state around for when it is used.
  return {
    gameState: { ...state.gameState, currentPlayerId: nextPlayerID },
  };
}
