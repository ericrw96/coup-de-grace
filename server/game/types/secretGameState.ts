import { ActionResponse } from "./gameEvents";
import { HiddenCard, PlayerID, ServerGameState } from "./gameTypes";
import { PossibleAction } from "./possibleAction";

export type SecretGameState =
  | RequestingPlayerResponses
  | RequestingInfluenceReveal;

export type InternalGameState = RequestingPlayerClaimResponses;

export interface RequestingPlayerClaimResponses {
  type: "claim_responses";
  onClaimLie: (
    state: ServerGameState
  ) => [InternalGameState | undefined, ServerGameState];
  onAllowed: (
    state: ServerGameState
  ) => [InternalGameState | undefined, ServerGameState];
}

export interface RequestingPlayerResponses {
  type: "player_response";
  responses: Array<PlayerResponse>;
  playerClaiming: PlayerID;
  cardClaimed?: HiddenCard;
  actionToBeDone: PossibleAction;
}

export interface PlayerResponse {
  response?: ActionResponse;
  playerID: PlayerID;
}

export interface RequestingInfluenceReveal {
  type: "influence_reveal";
  playerToReveal: PlayerID;
  cardClaimed?: HiddenCard;
  actionToBeDone: PossibleAction;
  otherPlayer?: PlayerID;
}
