import {PossibleAction} from "./possibleAction";

export type ClientActions = RespondToActionProposal | ProposeAction | RevealInfluence | SelectInfluence

export interface RespondToActionProposal{
 pendingAction: PossibleAction
}

export interface ProposeAction {

}

export interface RevealInfluence {

}

export interface SelectInfluence {

}