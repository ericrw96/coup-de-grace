import { HiddenCard, PlayerID } from "./gameTypes";
import { PossibleAction } from "./possibleAction";

export interface ReceivedGameEvent {
  event: GameEvent;
  playerEventIsFrom: PlayerID;
}

export type GameEvent =
  | BeginGame
  | ProposeAction
  | RespondToAction
  | RevealInfluence
  | RespondForBlock
  | SelectInfluence;

export interface BeginGame {
  type: "begin_game";
}

export interface ProposeAction {
  type: "propose_action";
  action: PossibleAction;
}

export interface RespondToAction {
  type: "respond_to_action";
  player: PlayerID;
  response: ActionResponse;
}

export interface RespondForBlock {
  type: "respond_for_block";
  response: BlockResponse;
}

export type ActionResponse = "claim_lie" | "allow";

export type BlockResponse = "allow" | { type: "block"; card: HiddenCard };

export interface RevealInfluence {
  type: "reveal_influence";
  cardToReveal: HiddenCard;
  player: PlayerID;
}

export interface SelectInfluence {
  type: "select_influence";
  influence_selected: Array<HiddenCard>;
}

export type LoggedGameEvent = string;
export type GameEventLogger = (message: LoggedGameEvent) => void;
