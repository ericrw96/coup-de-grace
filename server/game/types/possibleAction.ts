import { PlayerID } from "./gameTypes";

export interface PossibleAction {
  sourcePlayerID: PlayerID;
  targetPlayerID: PlayerID;
  action: PlayerEventAction;
}

export const playerEventActionsWithBlock = [
  "assassinate",
  "steal",
  "foreign_aid",
] as const;
export type PlayerEventActionWithBlock = typeof playerEventActionsWithBlock[number];

export const playerEventActions = [
  "exchange_with_court_deck",
  "tax",
  "income",
  "coup",
  ...playerEventActionsWithBlock,
] as const;
export type PlayerEventAction = typeof playerEventActions[number];
export const blockActions = [
  "block_aid",
  "block_steal",
  "block_assassin",
] as const;
export type BlockAction = typeof blockActions[number];

export type Action =
  | PlayerEventAction
  | BlockAction
  | PlayerEventActionWithBlock;
