import {
  AnyT,
  jsonArrayMember,
  jsonMember,
  jsonObject,
  toJson,
} from "typedjson";
import "reflect-metadata";
import {
  ServerGameState,
  GameState,
  HiddenInfluence,
  PlayerState,
  HiddenCard,
  InfluenceVisibility,
  PlayerID,
} from "./gameTypes";

@jsonObject
@toJson
export class ServerGameStateClass implements ServerGameState {
  @jsonMember(() => GameStateClass)
  gameState!: GameState<HiddenInfluence>;
  @jsonMember(AnyT)
  foreignAidState?: { blockers: string[] } | undefined;
  @jsonMember(AnyT)
  stealState?: { targetToStealFrom: string } | undefined;
  @jsonMember(AnyT)
  assassinateState?: { targetToAssassinate: string } | undefined;
}

@jsonObject
@toJson
export class HiddenInfluenceClass implements HiddenInfluence {
  @jsonMember(String)
  card!: HiddenCard;
  @jsonMember(AnyT)
  visibility!: InfluenceVisibility;
}

@jsonObject
@toJson
export class PlayerStateClass implements PlayerState<HiddenInfluence> {
  @jsonArrayMember(HiddenInfluenceClass)
  influence!: Array<HiddenInfluence>;
  @jsonMember(Number)
  coins!: number;
  @jsonMember(String)
  id!: PlayerID;
}

@jsonObject
@toJson
export class GameStateClass implements GameState<HiddenInfluence> {
  @jsonArrayMember(PlayerStateClass)
  players!: Array<PlayerStateClass>;
  @jsonArrayMember(AnyT)
  deck!: HiddenInfluence[];
  @jsonArrayMember(AnyT)
  revealedCards!: HiddenInfluence[];
  @jsonMember(String)
  currentPlayerId!: PlayerID;
}
