import { PlayerAction } from "./PlayerAction";

export type PlayerID = string;

export type ServerGameState = {
  gameState: GameState<HiddenInfluence>;
  foreignAidState?: { blockers: Array<PlayerID> };
  stealState?: { targetToStealFrom: PlayerID };
  assassinateState?: { targetToAssassinate: PlayerID };
};

export type ClientGameState = {
  gameState: GameState<Card>;
  playerAction: PlayerAction;
};

export interface GameState<T> {
  players: Array<PlayerState<T>>;
  deck: Array<T>;
  revealedCards: Array<T>;
  currentPlayerId: PlayerID;
}

export interface PlayerState<T> {
  influence: Array<T>;
  coins: number;
  id: PlayerID;
}

export interface HiddenInfluence {
  card: HiddenCard;
  visibility: InfluenceVisibility;
}

export interface VisibleToAll {
  type: "visible_to_all";
}

export interface NotVisible {
  type: "not_visible";
}

export interface VisibleToPlayer {
  type: "visible_to_player";
  visibleToPlayerId: PlayerID;
}

export type InfluenceVisibility = VisibleToAll | NotVisible | VisibleToPlayer;

export type Card = "NOT_KNOWN" | HiddenCard;

export type HiddenCard =
  | "DUKE"
  | "ASSASSIN"
  | "CONTESSA"
  | "CAPTAIN"
  | "AMBASSADOR";
