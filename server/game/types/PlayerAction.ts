import { HiddenCard, PlayerID } from "./gameTypes";

export type PlayerClaim = {
  playerClaiming: PlayerID;
  cardClaimed: HiddenCard;
};
export type AdditionalInfluence = {
  cards: Array<HiddenCard>;
};

export type Block = {
  possible: Array<HiddenCard>;
  playerToBlock: PlayerID;
};

export type PlayerAction =
  | { type: "WAIT_FOR_OTHERS" }
  | { type: "SELECT_ACTION" }
  | { type: "RESPOND_TO_CLAIM"; claim: PlayerClaim }
  | { type: "REVEAL_INFLUENCE" }
  | { type: "BLOCK_FOREIGN_AID"; block: Block }
  | { type: "SELECT_INFLUENCE"; additionalInfluence: AdditionalInfluence }
  | {
      type: "CHOOSE_BLOCK";
      block: Block;
    }
  | { type: "GAME_OVER" };
