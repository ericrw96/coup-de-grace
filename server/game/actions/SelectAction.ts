import { ActionState } from "./base/ActionState";
import { PlayerID, ServerGameState } from "../types/gameTypes";
import { GameEventLogger, ReceivedGameEvent } from "../types/gameEvents";
import assert from "assert";
import { changePlayerMoney } from "../state/StateModifications";
import { ForeignAidAction } from "./foreignAid/ForeignAidAction";
import { ExchangeWithDeckAction } from "./exchange/ExchangeWithDeckAction";
import { StealAction } from "./steal/StealAction";
import { AssassinateAction } from "./assassinate/AssassinateAction";
import { isPlayerDead } from "../state/StateUtils";
import { getFunctionX } from "./influence/InfluenceRevealFunctions";
import { PlayerAction } from "../types/PlayerAction";
import forcedInfluenceRevealAction from "./influence/ForcedInfluenceRevealAction";
import { actionState, BaseActionState } from "./base/BaseActionState";
import { TaxAction } from "./tax/TaxAction";

@actionState
export class SelectAction extends BaseActionState {
  private constructor(state?: ServerGameState) {
    super(state);
  }
  static create(state: ServerGameState) {
    return new SelectAction(state);
  }
  nextState(
    { event, playerEventIsFrom }: ReceivedGameEvent,
    logger: GameEventLogger,
  ): ActionState {
    assert(event.type === "propose_action");
    const currentPlayerID = this.getCurrentPlayer();
    assert(currentPlayerID === playerEventIsFrom);
    const currentPlayer = this.getCurrentPlayerObject();
    assert(currentPlayer !== undefined);
    const { action, sourcePlayerID, targetPlayerID } = event.action;
    assert(sourcePlayerID === playerEventIsFrom);
    assert(currentPlayer.coins < 10 || action === "coup");
    assert(!isPlayerDead(this.state, targetPlayerID));

    switch (action) {
      case "income": {
        assert(sourcePlayerID === targetPlayerID);
        const [newState] = changePlayerMoney(this.state, currentPlayerID, 1);
        logger(`${sourcePlayerID} took income`);
        return getFunctionX("GO_NEXT_PLAYER")(newState, logger);
      }
      case "coup": {
        assert(sourcePlayerID !== targetPlayerID);
        assert(currentPlayer.coins >= 7);
        const [newState] = changePlayerMoney(this.state, currentPlayerID, -7);
        logger(
          `${sourcePlayerID} has lauched a coup against ${targetPlayerID}`,
        );
        return forcedInfluenceRevealAction(newState, targetPlayerID);
      }
      case "steal":
        assert(sourcePlayerID !== targetPlayerID);
        this.state.stealState = { targetToStealFrom: targetPlayerID };
        logger(`${sourcePlayerID} would like to steal from ${targetPlayerID}`);
        return StealAction.create(this.state, new Map());
      case "assassinate":
        assert(sourcePlayerID !== targetPlayerID);
        this.state.assassinateState = { targetToAssassinate: targetPlayerID };
        logger(`${sourcePlayerID} would like to assasinate ${targetPlayerID}`);
        return AssassinateAction.create(this.state, new Map());
      case "foreign_aid": {
        assert(sourcePlayerID === targetPlayerID);
        logger(`${sourcePlayerID} would like to take foreign aid`);
        return ForeignAidAction.create(this.state);
      }
      case "exchange_with_court_deck":
        assert(sourcePlayerID === targetPlayerID);
        logger(`${sourcePlayerID} would like to exchange with the court deck`);
        return ExchangeWithDeckAction.create(this.state, new Map());
      case "tax": {
        assert(sourcePlayerID === targetPlayerID);
        logger(`${sourcePlayerID} would like to tax`);
        return TaxAction.create(this.state, new Map());
      }
    }
  }

  getPlayerActions(): Map<PlayerID, PlayerAction> {
    const playerMap: Map<PlayerID, PlayerAction> = new Map();
    for (const player of this.livingPlayers) {
      if (player.id === this.getCurrentPlayer()) {
        playerMap.set(player.id, { type: "SELECT_ACTION" });
      } else {
        playerMap.set(player.id, { type: "WAIT_FOR_OTHERS" });
      }
    }
    return playerMap;
  }
  getCurrentPlayer(): PlayerID {
    return this.state.gameState.currentPlayerId;
  }
}
