import { HiddenCard, PlayerID, ServerGameState } from "../../types/gameTypes";
import { InfluenceRevealFunctionType } from "../influence/InfluenceRevealFunctions";
import { ActionStateWithSingleBlock } from "../base/ActionStateWithSingleBlock";
import { ActionResponse } from "../../types/gameEvents";
import { actionState } from "../base/BaseActionState";

@actionState
export class TaxAction extends ActionStateWithSingleBlock {
  createNew(): TaxAction {
    return new TaxAction(this.state, this.responses);
  }

  public static create(
    state: ServerGameState,
    responses: Map<PlayerID, ActionResponse>,
    playerThatMadeClaim: PlayerID | undefined = undefined,
  ) {
    return new TaxAction(state, responses, playerThatMadeClaim);
  }

  readonly onSuccessfulClaim: InfluenceRevealFunctionType = "TAX";
  readonly correctCard: HiddenCard = "DUKE";
}
