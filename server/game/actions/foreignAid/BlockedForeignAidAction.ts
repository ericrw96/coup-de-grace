import { PlayerID, ServerGameState } from "../../types/gameTypes";
import { ActionResponse } from "../../types/gameEvents";
import { ActionStateWithSingleBlock } from "../base/ActionStateWithSingleBlock";
import { jsonMember } from "typedjson";
import assert from "assert";
import { actionState } from "../base/BaseActionState";

@actionState
export class BlockedForeignAidAction extends ActionStateWithSingleBlock {
  private constructor(
    state?: ServerGameState,
    responses?: Map<PlayerID, ActionResponse>,
    blockingPlayer?: PlayerID,
  ) {
    super(state, responses, blockingPlayer);
    this._blockingPlayer = blockingPlayer;
  }

  public static create(
    state: ServerGameState,
    responses: Map<PlayerID, ActionResponse>,
    blockingPlayer: PlayerID,
  ) {
    return new BlockedForeignAidAction(state, responses, blockingPlayer);
  }

  createNew(): ActionStateWithSingleBlock {
    return new BlockedForeignAidAction(
      this.state,
      this.responses,
      this.blockingPlayer,
    );
  }
  readonly onSuccessfulClaim = "GO_NEXT_PLAYER";
  readonly onFailedClaim = "FOREIGN_AID_BLOCK";

  readonly correctCard = "DUKE";
  @jsonMember(String)
  private readonly _blockingPlayer?: PlayerID;
  get blockingPlayer(): PlayerID {
    assert(this._blockingPlayer !== undefined);
    return this._blockingPlayer;
  }
}
