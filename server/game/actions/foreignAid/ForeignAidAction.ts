import { ActionState } from "../base/ActionState";
import { PlayerID, ServerGameState } from "../../types/gameTypes";
import {
  BlockResponse,
  GameEventLogger,
  ReceivedGameEvent,
} from "../../types/gameEvents";
import assert from "assert";
import { getFunctionX } from "../influence/InfluenceRevealFunctions";
import { PlayerAction } from "../../types/PlayerAction";
import { actionState, BaseActionState } from "../base/BaseActionState";
import { AnyT, jsonMapMember } from "typedjson";

@actionState
export class ForeignAidAction extends BaseActionState {
  @jsonMapMember(String, AnyT)
  private readonly responses: Map<PlayerID, BlockResponse>;

  private constructor(
    state?: ServerGameState,
    responses?: Map<PlayerID, BlockResponse>,
  ) {
    super(state);
    this.responses = responses ?? new Map();
  }
  public static create(
    state: ServerGameState,
    responses: Map<PlayerID, BlockResponse> = new Map(),
  ) {
    return new ForeignAidAction(state, responses);
  }
  getPlayerActions(): Map<PlayerID, PlayerAction> {
    const playerActions: Map<PlayerID, PlayerAction> = new Map();
    for (const { id } of this.livingPlayers) {
      if (this.responses.get(id) !== undefined) {
        playerActions.set(id, { type: "WAIT_FOR_OTHERS" });
      } else if (id === this.getCurrentPlayer()) {
        playerActions.set(id, { type: "WAIT_FOR_OTHERS" });
      } else {
        playerActions.set(id, {
          type: "BLOCK_FOREIGN_AID",
          block: { possible: ["DUKE"], playerToBlock: this.getCurrentPlayer() },
        });
      }
    }
    return playerActions;
  }
  get finishedResponding(): boolean {
    return this.responses.size === this.livingPlayers.length - 1;
  }

  nextState(
    { event, playerEventIsFrom }: ReceivedGameEvent,
    logger: GameEventLogger,
  ): ActionState {
    assert(event.type === "respond_for_block");
    assert(playerEventIsFrom !== this.getCurrentPlayer());
    assert(this.responses.get(playerEventIsFrom) === undefined);
    this.responses.set(playerEventIsFrom, event.response);
    if (this.finishedResponding) {
      const blockResponses: Array<PlayerID> = [];
      for (const [id, playerResponse] of this.responses) {
        if (playerResponse === "allow") {
          continue;
        }
        blockResponses.push(id);
      }
      if (blockResponses.length === 0) {
        return getFunctionX("FOREIGN_AID")(this.state, logger);
      }
      this.state.foreignAidState = { blockers: blockResponses };
      logger(
        `The foreign aid action is blocked by ${blockResponses.join(
          " and ",
        )} claiming DUKE`,
      );
      return getFunctionX("FOREIGN_AID_BLOCK")(this.state, logger);
    }
    return ForeignAidAction.create(this.state, this.responses);
  }
}
