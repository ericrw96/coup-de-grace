import { ActionState } from "../base/ActionState";
import {
  HiddenCard,
  HiddenInfluence,
  PlayerID,
  ServerGameState,
} from "../../types/gameTypes";
import { GameEventLogger, ReceivedGameEvent } from "../../types/gameEvents";
import assert from "assert";
import { getFunctionX } from "../influence/InfluenceRevealFunctions";
import { PlayerAction } from "../../types/PlayerAction";
import { actionState, BaseActionState } from "../base/BaseActionState";
import { shuffleArray } from "../../utils/createGameUtils";
import { jsonArrayMember } from "typedjson";

@actionState
export class SelectInfluenceAction extends BaseActionState {
  @jsonArrayMember(String)
  private readonly _additional_cards?: [HiddenCard, HiddenCard];
  get additional_cards(): [HiddenCard, HiddenCard] {
    assert(this._additional_cards !== undefined);
    return this._additional_cards;
  }
  private constructor(
    state?: ServerGameState,
    additional_cards?: [HiddenCard, HiddenCard],
  ) {
    super(state);
    this._additional_cards = additional_cards;
  }
  public static create(
    state: ServerGameState,
    additional_cards: [HiddenCard, HiddenCard],
  ) {
    return new SelectInfluenceAction(state, additional_cards);
  }
  get possibleCards(): Array<HiddenCard> {
    return this.getCurrentPlayerObject()
      .influence.map((inf) => inf.card)
      .concat(this.additional_cards);
  }
  getPlayerActions(): Map<PlayerID, PlayerAction> {
    const playerActions: Map<PlayerID, PlayerAction> = new Map();
    for (const { id } of this.livingPlayers) {
      if (id === this.getCurrentPlayer()) {
        playerActions.set(id, {
          type: "SELECT_INFLUENCE",
          additionalInfluence: { cards: this.additional_cards },
        });
      } else {
        playerActions.set(id, { type: "WAIT_FOR_OTHERS" });
      }
    }
    return playerActions;
  }

  nextState(
    { event, playerEventIsFrom }: ReceivedGameEvent,
    logger: GameEventLogger,
  ): ActionState {
    assert(event.type === "select_influence");
    assert(playerEventIsFrom === this.getCurrentPlayer());
    const { influence_selected } = event;
    assert(
      influence_selected.length ===
        this.getCurrentPlayerObject().influence.length,
    );
    const possibleCards = this.possibleCards;
    for (const influenceSelectedElement of influence_selected) {
      const index = possibleCards.findIndex(
        (card) => influenceSelectedElement === card,
      );
      assert(index !== -1);
      possibleCards.splice(index, 1);
    }
    this.getCurrentPlayerObject().influence = influence_selected.map(
      (card) => ({
        card,
        visibility: {
          type: "visible_to_player",
          visibleToPlayerId: this.getCurrentPlayer(),
        },
      }),
    );
    const influencesToPutBack: Array<HiddenInfluence> = possibleCards.map(
      (card) => ({
        card,
        visibility: { type: "not_visible" },
      }),
    );
    this.state.gameState.deck = shuffleArray([
      ...this.state.gameState.deck,
      ...influencesToPutBack,
    ]);
    logger(`${this.getCurrentPlayer()} exchanged with the court deck.`);
    return getFunctionX("GO_NEXT_PLAYER")(this.state, logger);
  }
}
