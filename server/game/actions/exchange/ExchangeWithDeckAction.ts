import { HiddenCard, PlayerID, ServerGameState } from "../../types/gameTypes";
import { InfluenceRevealFunctionType } from "../influence/InfluenceRevealFunctions";
import { ActionStateWithSingleBlock } from "../base/ActionStateWithSingleBlock";
import { ActionResponse } from "../../types/gameEvents";
import { actionState } from "../base/BaseActionState";

@actionState
export class ExchangeWithDeckAction extends ActionStateWithSingleBlock {
  createNew(): ExchangeWithDeckAction {
    return new ExchangeWithDeckAction(this.state, this.responses);
  }

  readonly onSuccessfulClaim: InfluenceRevealFunctionType = "EXCHANGE_DECK";
  readonly correctCard: HiddenCard = "AMBASSADOR";

  public static create(
    state: ServerGameState,
    responses: Map<PlayerID, ActionResponse>,
  ) {
    return new ExchangeWithDeckAction(state, responses);
  }
}
