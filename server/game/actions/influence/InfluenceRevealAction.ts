import { ActionState } from "../base/ActionState";
import { HiddenCard, PlayerID, ServerGameState } from "../../types/gameTypes";
import { GameEventLogger, ReceivedGameEvent } from "../../types/gameEvents";
import assert from "assert";
import {
  getFunction,
  getFunctionX,
  InfluenceRevealFunctionType,
} from "./InfluenceRevealFunctions";
import { getPlayer } from "../../state/StateUtils";
import { shuffleArray } from "../../utils/createGameUtils";
import { PlayerAction } from "../../types/PlayerAction";
import forcedInfluenceRevealAction from "./ForcedInfluenceRevealAction";
import { actionState, BaseActionState } from "../base/BaseActionState";
import { jsonMember } from "typedjson";

@actionState
export class InfluenceRevealAction extends BaseActionState {
  @jsonMember(String)
  private readonly correctCard: HiddenCard | undefined;
  @jsonMember(String)
  private readonly _playerID?: PlayerID;
  get playerID(): PlayerID {
    assert(this._playerID !== undefined);
    return this._playerID;
  }
  @jsonMember(String)
  private readonly playerToPenalize: PlayerID | undefined;
  @jsonMember(String)
  private readonly onCorrectShowing: InfluenceRevealFunctionType | undefined;
  @jsonMember(String)
  private readonly onFailedShowing: InfluenceRevealFunctionType | undefined;
  private constructor(
    state?: ServerGameState,
    playerID?: PlayerID,
    onCorrectShowing?: InfluenceRevealFunctionType,
    correctCard?: HiddenCard,
    playerToPenalize?: PlayerID,
    onFailedShowing?: InfluenceRevealFunctionType,
  ) {
    super(state);
    this.onCorrectShowing = onCorrectShowing;
    this.correctCard = correctCard;
    this._playerID = playerID;
    this.playerToPenalize = playerToPenalize;
    this.onFailedShowing = onFailedShowing;
  }
  public static create(
    state: ServerGameState,
    playerID: PlayerID,
    onCorrectShowing: InfluenceRevealFunctionType | undefined = undefined,
    correctCard: HiddenCard | undefined = undefined,
    playerToPenalize: PlayerID | undefined = undefined,
    onFailedShowing: InfluenceRevealFunctionType | undefined = undefined,
  ) {
    return new InfluenceRevealAction(
      state,
      playerID,
      onCorrectShowing,
      correctCard,
      playerToPenalize,
      onFailedShowing,
    );
  }
  getPlayerActions(): Map<PlayerID, PlayerAction> {
    const playerActions: Map<PlayerID, PlayerAction> = new Map();
    for (const { id } of this.livingPlayers) {
      if (this.playerID === id) {
        playerActions.set(id, { type: "REVEAL_INFLUENCE" });
      } else {
        playerActions.set(id, { type: "WAIT_FOR_OTHERS" });
      }
    }
    return playerActions;
  }

  nextState(
    { event, playerEventIsFrom }: ReceivedGameEvent,
    logger: GameEventLogger,
  ): ActionState {
    assert(event.type === "reveal_influence");
    assert(event.player === this.playerID);
    assert(event.player === playerEventIsFrom);
    const revealingPlayer = getPlayer(this.state, this.playerID);
    // TODO allow this to continue when the player is dead
    const index = revealingPlayer.influence.findIndex(
      (influence) => influence.card === event.cardToReveal,
    );
    assert(index !== -1);
    revealingPlayer.influence.splice(index, 1);
    if (event.cardToReveal === this.correctCard) {
      // then shuffle back in and penalize the other player
      this.state.gameState.deck.push({
        card: event.cardToReveal,
        visibility: { type: "not_visible" },
      });
      const newDeck = shuffleArray(this.state.gameState.deck);
      const newCard = newDeck.pop();
      assert(newCard !== undefined);
      this.state.gameState.deck = newDeck;
      revealingPlayer.influence = [
        ...revealingPlayer.influence,
        {
          ...newCard,
          visibility: {
            type: "visible_to_player",
            visibleToPlayerId: revealingPlayer.id,
          },
        },
      ];
      logger(
        `${this.playerID} revealed their ${this.correctCard} and drew a new influence`,
      );
      if (this.playerToPenalize !== undefined) {
        logger(
          `${this.playerToPenalize} must now reveal an influence as penalty.`,
        );
        return forcedInfluenceRevealAction(
          this.state,
          this.playerToPenalize,
          this.onCorrectShowing,
        );
      } else {
        return (
          getFunction(this.onCorrectShowing)?.(this.state, logger) ??
          getFunctionX("GO_NEXT_PLAYER")(this.state, logger)
        );
      }
    } else {
      logger(`${this.playerID} revealed ${event.cardToReveal}`);
      // add card to revealed set
      this.state.gameState.revealedCards.push({
        card: event.cardToReveal,
        visibility: { type: "visible_to_all" },
      });
      return (
        getFunction(this.onFailedShowing)?.(this.state, logger) ??
        getFunctionX("GO_NEXT_PLAYER")(this.state, logger)
      );
    }
  }
}
