import { HiddenInfluence, ServerGameState } from "../../types/gameTypes";
import { ActionState } from "../base/ActionState";
import assert from "assert";
import { SelectAction } from "../SelectAction";
import {
  changePlayerMoney,
  goToNextPlayer,
} from "../../state/StateModifications";
import { BlockedForeignAidAction } from "../foreignAid/BlockedForeignAidAction";
import { SelectInfluenceAction } from "../exchange/SelectInfluenceAction";
import { StealAskBlockersState } from "../steal/StealAskBlockersState";
import { AssassinateAskBlockersState } from "../assassinate/AssassinateAskBlockersState";
import { isPlayerDead } from "../../state/StateUtils";
import forcedInfluenceRevealAction from "./ForcedInfluenceRevealAction";
import { GameEventLogger } from "../../types/gameEvents";

export const InfluenceRevealFunctions = {
  TAX: tax_function,
  FOREIGN_AID: foreign_aid_function,
  FOREIGN_AID_BLOCK: foreign_aid_block_function,
  EXCHANGE_DECK: exchange_deck_function,
  STEAL: steal_function,
  STEAL_ASK_BLOCKERS: steal_ask_blockers_function,
  ASSASSINATE: assassinate_function,
  ASSASSINATE_ASK_BLOCKER: assassinate_ask_blocker_function,
  GO_NEXT_PLAYER: go_next_player_function,
} as const;

export type InfluenceRevealFunctionType = keyof typeof InfluenceRevealFunctions;

export type InfluenceRevealFunction = typeof InfluenceRevealFunctions[InfluenceRevealFunctionType];

export function getFunction(
  type: InfluenceRevealFunctionType | undefined,
): InfluenceRevealFunction | undefined {
  if (type === undefined) {
    return undefined;
  }
  return getFunctionX(type);
}

export function getFunctionX(
  type: InfluenceRevealFunctionType,
): InfluenceRevealFunction {
  return InfluenceRevealFunctions[type];
}

function tax_function(
  state: ServerGameState,
  logger: GameEventLogger,
): ActionState {
  const [newState] = changePlayerMoney(
    state,
    state.gameState.currentPlayerId,
    3,
  );
  logger(`${state.gameState.currentPlayerId} took tax`);
  return go_next_player_function(newState, logger);
}

function foreign_aid_function(
  state: ServerGameState,
  logger: GameEventLogger,
): ActionState {
  const [newState] = changePlayerMoney(
    state,
    state.gameState.currentPlayerId,
    2,
  );
  logger(`${state.gameState.currentPlayerId} took foreign aid`);
  return go_next_player_function(newState, logger);
}

function foreign_aid_block_function(
  state: ServerGameState,
  logger: GameEventLogger,
): ActionState {
  const { foreignAidState } = state;
  assert(foreignAidState !== undefined);
  if (foreignAidState.blockers.length === 0) {
    return foreign_aid_function(state, logger);
  }
  const newBlocker = foreignAidState.blockers.shift();
  assert(newBlocker !== undefined);
  if (isPlayerDead(state, newBlocker)) {
    return foreign_aid_block_function(state, logger);
  }
  return BlockedForeignAidAction.create(state, new Map(), newBlocker);
}

function exchange_deck_function(state: ServerGameState): ActionState {
  const cards = [state.gameState.deck.pop(), state.gameState.deck.pop()]
    .filter((card): card is HiddenInfluence => !!card)
    .map((inf) => inf.card);
  return SelectInfluenceAction.create(state, [cards[0], cards[1]]);
}
function steal_function(
  state: ServerGameState,
  logger: GameEventLogger,
): ActionState {
  const { stealState } = state;
  assert(stealState !== undefined);
  state.stealState = undefined;
  const [minusState, moneyDeducted] = changePlayerMoney(
    state,
    stealState.targetToStealFrom,
    -2,
    true,
  );
  const [stolenState] = changePlayerMoney(
    minusState,
    minusState.gameState.currentPlayerId,
    Math.abs(moneyDeducted),
  );
  logger(
    `${minusState.gameState.currentPlayerId} took ${moneyDeducted} coins from ${stealState.targetToStealFrom}`,
  );
  return go_next_player_function(stolenState, logger);
}

function steal_ask_blockers_function(
  state: ServerGameState,
  logger: GameEventLogger,
): ActionState {
  const { stealState } = state;
  assert(stealState !== undefined);
  if (isPlayerDead(state, stealState.targetToStealFrom)) {
    return steal_function(state, logger);
  }
  return (() =>
    StealAskBlockersState.create(state, stealState.targetToStealFrom))();
}

function assassinate_function(
  state: ServerGameState,
  logger: GameEventLogger,
): ActionState {
  const { assassinateState } = state;
  assert(assassinateState !== undefined);
  const [newState] = changePlayerMoney(
    state,
    state.gameState.currentPlayerId,
    -3,
  );
  if (isPlayerDead(newState, assassinateState.targetToAssassinate)) {
    return go_next_player_function(newState, logger);
  }
  return forcedInfluenceRevealAction(
    newState,
    assassinateState.targetToAssassinate,
  );
}

function assassinate_ask_blocker_function(
  state: ServerGameState,
  logger: GameEventLogger,
): ActionState {
  const { assassinateState } = state;
  assert(assassinateState !== undefined);
  if (isPlayerDead(state, assassinateState.targetToAssassinate)) {
    return assassinate_function(state, logger);
  }
  return (() =>
    AssassinateAskBlockersState.create(
      state,
      assassinateState.targetToAssassinate,
    ))();
}

function go_next_player_function(
  state: ServerGameState,
  logger: GameEventLogger,
): ActionState {
  const nextState = goToNextPlayer(state);
  logger(`it is now ${state.gameState.currentPlayerId}'s turn`);
  return SelectAction.create(nextState);
}
