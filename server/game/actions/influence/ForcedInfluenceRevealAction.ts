import { InfluenceRevealAction } from "./InfluenceRevealAction";
import { PlayerID, ServerGameState } from "../../types/gameTypes";
import { InfluenceRevealFunctionType } from "./InfluenceRevealFunctions";

export default function forcedInfluenceRevealAction(
  state: ServerGameState,
  playerToForce: PlayerID,
  onReveal: InfluenceRevealFunctionType | undefined = undefined,
): InfluenceRevealAction {
  return InfluenceRevealAction.create(
    state,
    playerToForce,
    undefined,
    undefined,
    undefined,
    onReveal,
  );
}
