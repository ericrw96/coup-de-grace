import { PlayerID, ServerGameState } from "../../types/gameTypes";
import { GameEventLogger, ReceivedGameEvent } from "../../types/gameEvents";
import { PlayerAction } from "../../types/PlayerAction";

export interface ActionState {
  state: ServerGameState;
  nextState: (
    { event, playerEventIsFrom }: ReceivedGameEvent,
    logger: GameEventLogger
  ) => ActionState;
  getPlayerActions: () => Map<PlayerID, PlayerAction>;
}
