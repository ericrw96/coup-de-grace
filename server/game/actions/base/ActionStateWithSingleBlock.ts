import { HiddenCard, PlayerID, ServerGameState } from "../../types/gameTypes";
import {
  ActionResponse,
  GameEventLogger,
  ReceivedGameEvent,
} from "../../types/gameEvents";
import { PlayerAction } from "../../types/PlayerAction";
import { InfluenceRevealAction } from "../influence/InfluenceRevealAction";
import {
  getFunctionX,
  InfluenceRevealFunctionType,
} from "../influence/InfluenceRevealFunctions";
import assert from "assert";
import { ActionState } from "./ActionState";
import { actionState, BaseActionState } from "./BaseActionState";
import { AnyT, jsonMapMember, jsonMember } from "typedjson";

@actionState
export abstract class ActionStateWithSingleBlock extends BaseActionState {
  get responses(): Map<PlayerID, ActionResponse> {
    return this._responses;
  }

  @jsonMapMember(String, AnyT)
  private readonly _responses: Map<PlayerID, ActionResponse>;
  @jsonMember(String)
  protected readonly _playerThatMadeClaim?: PlayerID;

  get playerThatMadeClaim(): PlayerID {
    return this._playerThatMadeClaim ?? this.getCurrentPlayer();
  }

  protected constructor(
    state: ServerGameState | undefined = undefined,
    responses: Map<PlayerID, ActionResponse> | undefined = undefined,
    playerThatMadeClaim: PlayerID | undefined = undefined,
  ) {
    super(state);
    this._responses = responses ?? new Map();
    this._playerThatMadeClaim = playerThatMadeClaim;
  }

  get playerWhoClaimedLie(): PlayerID | undefined {
    for (const [id, response] of this.responses) {
      if (response === "claim_lie") {
        return id;
      }
    }
    return undefined;
  }

  get finishedResponding(): boolean {
    return (
      this.responses.size === this.livingPlayers.length - 1 ||
      this.playerWhoClaimedLie !== undefined
    );
  }

  getPlayerActions(): Map<PlayerID, PlayerAction> {
    const playerActions: Map<PlayerID, PlayerAction> = new Map();
    for (const { id } of this.livingPlayers) {
      if (this.responses.get(id) !== undefined) {
        playerActions.set(id, { type: "WAIT_FOR_OTHERS" });
      } else if (id === this.playerThatMadeClaim) {
        playerActions.set(id, { type: "WAIT_FOR_OTHERS" });
      } else {
        playerActions.set(id, {
          type: "RESPOND_TO_CLAIM",
          claim: {
            playerClaiming: this.playerThatMadeClaim,
            cardClaimed: this.correctCard,
          },
        });
      }
    }
    return playerActions;
  }

  onClaimLie(playerWhoClaimedLie: PlayerID): ActionState {
    return InfluenceRevealAction.create(
      this.state,
      this.playerThatMadeClaim,
      this.onSuccessfulClaim,
      this.correctCard,
      playerWhoClaimedLie,
      this.onFailedClaim,
    );
  }

  abstract createNew(): ActionStateWithSingleBlock;

  @jsonMember(String)
  abstract readonly onSuccessfulClaim: InfluenceRevealFunctionType;
  @jsonMember(String)
  readonly onFailedClaim: InfluenceRevealFunctionType = "GO_NEXT_PLAYER";
  @jsonMember(String)
  abstract readonly correctCard: HiddenCard;

  nextState(
    { event, playerEventIsFrom }: ReceivedGameEvent,
    logger: GameEventLogger,
  ): ActionState {
    assert(event.type === "respond_to_action");
    assert(event.player === playerEventIsFrom);
    assert(event.player !== this.playerThatMadeClaim);
    assert(this.responses.get(event.player) === undefined);
    this.responses.set(event.player, event.response);
    if (this.finishedResponding) {
      const playerWhoClaimedLie = this.playerWhoClaimedLie;
      if (playerWhoClaimedLie !== undefined) {
        logger(
          `${playerWhoClaimedLie} challenged ${this.playerThatMadeClaim}'s claim`,
        );
        return this.onClaimLie(playerWhoClaimedLie);
      } else {
        return getFunctionX(this.onSuccessfulClaim)(this.state, logger);
      }
    } else {
      return this.createNew();
    }
  }
}
