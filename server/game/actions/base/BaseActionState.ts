import {
  HiddenInfluence,
  PlayerID,
  PlayerState,
  ServerGameState,
} from "../../types/gameTypes";
import { PlayerAction } from "../../types/PlayerAction";
import { GameEventLogger, ReceivedGameEvent } from "../../types/gameEvents";
import assert from "assert";
import { ActionState } from "./ActionState";
import {
  IJsonObjectOptions,
  jsonMember,
  jsonObject,
  JsonObjectMetadata,
  Serializable,
} from "typedjson";
import { ServerGameStateClass } from "../../types/classesForTypedJson";

const META_FIELD = "__typedJsonJsonObjectMetadataInformation__";

function findRootMetaInfo(proto: any): JsonObjectMetadata {
  const protoProto = Object.getPrototypeOf(proto);
  if (!protoProto || !protoProto[META_FIELD]) {
    return proto[META_FIELD];
  }
  return findRootMetaInfo(protoProto);
}

export function actionState<T>(target: Serializable<T>): void;
export function actionState<T extends Object>(
  options?: IJsonObjectOptions<T> | Serializable<T>,
): ((target: Serializable<T>) => void) | void {
  if (typeof options === "function") {
    const target = options;
    jsonObject({ ...options })(target as Serializable<T>);
    findRootMetaInfo(target.prototype).knownTypes.add(target);
    return;
  } else {
    return (target: Serializable<T>) => {
      jsonObject({ ...options })(target as Serializable<T>);
      findRootMetaInfo(target.prototype).knownTypes.add(target);
    };
  }
}

@actionState
export abstract class BaseActionState implements ActionState {
  protected constructor(state?: ServerGameState | undefined) {
    if (state !== undefined) {
      this._state = state;
    }
  }

  get state(): ServerGameState {
    assert(this._state !== undefined);
    return this._state;
  }

  set state(value: ServerGameState) {
    this._state = value;
  }

  get livingPlayers(): Array<PlayerState<HiddenInfluence>> {
    return this.state.gameState.players.filter(
      (player) => player.influence.length !== 0,
    );
  }

  abstract getPlayerActions(): Map<PlayerID, PlayerAction>;

  abstract nextState(
    { event, playerEventIsFrom }: ReceivedGameEvent,
    logger: GameEventLogger,
  ): ActionState;

  @jsonMember(ServerGameStateClass)
  private _state?: ServerGameStateClass;

  getCurrentPlayer(): PlayerID {
    return this.state.gameState.currentPlayerId;
  }

  getCurrentPlayerObject(): PlayerState<HiddenInfluence> {
    const player = this.state.gameState.players.find(
      ({ id }) => id === this.getCurrentPlayer(),
    );
    assert(player !== undefined);
    return player;
  }
}
