import { PlayerID, ServerGameState } from "../../types/gameTypes";
import { InfluenceRevealFunctionType } from "../influence/InfluenceRevealFunctions";
import { ActionResponse } from "../../types/gameEvents";
import { ActionStateWithSingleBlock } from "../base/ActionStateWithSingleBlock";
import assert from "assert";
import { actionState } from "../base/BaseActionState";

@actionState
export class BlockedStealState extends ActionStateWithSingleBlock {
  private constructor(
    state?: ServerGameState,
    responses?: Map<PlayerID, ActionResponse>,
    playerBlockingSteal?: PlayerID,
    card?: "AMBASSADOR" | "CAPTAIN",
  ) {
    super(state, responses, playerBlockingSteal);
    this._correctCard = card;
  }
  readonly _correctCard?: "AMBASSADOR" | "CAPTAIN";
  get correctCard(): "AMBASSADOR" | "CAPTAIN" {
    assert(this._correctCard !== undefined);
    return this._correctCard;
  }
  readonly onSuccessfulClaim: InfluenceRevealFunctionType = "GO_NEXT_PLAYER";
  readonly onFailedClaim: InfluenceRevealFunctionType = "STEAL";

  createNew(): ActionStateWithSingleBlock {
    return new BlockedStealState(
      this.state,
      this.responses,
      this.playerThatMadeClaim,
      this.correctCard,
    );
  }
  public static create(
    state: ServerGameState,
    responses: Map<PlayerID, ActionResponse>,
    playerBlockingSteal: PlayerID,
    card: "AMBASSADOR" | "CAPTAIN",
  ) {
    return new BlockedStealState(state, responses, playerBlockingSteal, card);
  }
}
