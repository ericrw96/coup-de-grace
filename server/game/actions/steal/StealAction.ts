import { HiddenCard, PlayerID, ServerGameState } from "../../types/gameTypes";
import { InfluenceRevealFunctionType } from "../influence/InfluenceRevealFunctions";
import { ActionStateWithSingleBlock } from "../base/ActionStateWithSingleBlock";
import { ActionResponse } from "../../types/gameEvents";
import { actionState } from "../base/BaseActionState";

@actionState
export class StealAction extends ActionStateWithSingleBlock {
  readonly correctCard: HiddenCard = "CAPTAIN";
  readonly onSuccessfulClaim: InfluenceRevealFunctionType =
    "STEAL_ASK_BLOCKERS";

  createNew(): ActionStateWithSingleBlock {
    return new StealAction(this.state, this.responses);
  }
  public static create(
    state: ServerGameState,
    responses: Map<PlayerID, ActionResponse>,
    playerThatMadeClaim: PlayerID | undefined = undefined,
  ) {
    return new StealAction(state, responses, playerThatMadeClaim);
  }
}
