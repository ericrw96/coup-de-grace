import { ActionState } from "../base/ActionState";
import { PlayerID, ServerGameState } from "../../types/gameTypes";
import { GameEventLogger, ReceivedGameEvent } from "../../types/gameEvents";
import assert from "assert";
import { getFunctionX } from "../influence/InfluenceRevealFunctions";
import { BlockedStealState } from "./BlockedStealState";
import { PlayerAction } from "../../types/PlayerAction";
import { actionState, BaseActionState } from "../base/BaseActionState";
import { jsonMember } from "typedjson";

@actionState
export class StealAskBlockersState extends BaseActionState {
  @jsonMember(String)
  private readonly _playerTargeted?: PlayerID;

  get playerTargeted(): PlayerID {
    assert(this._playerTargeted !== undefined);
    return this._playerTargeted;
  }

  public static create(state: ServerGameState, playerTargeted: PlayerID) {
    return new StealAskBlockersState(state, playerTargeted);
  }

  private constructor(state?: ServerGameState, playerTargeted?: PlayerID) {
    super(state);
    this._playerTargeted = playerTargeted;
  }

  getPlayerActions(): Map<PlayerID, PlayerAction> {
    const playerActions: Map<PlayerID, PlayerAction> = new Map();
    for (const { id } of this.livingPlayers) {
      if (id === this.playerTargeted) {
        playerActions.set(id, {
          type: "CHOOSE_BLOCK",
          block: {
            possible: ["CAPTAIN", "AMBASSADOR"],
            playerToBlock: this.getCurrentPlayer(),
          },
        });
      } else {
        playerActions.set(id, { type: "WAIT_FOR_OTHERS" });
      }
    }
    return playerActions;
  }

  nextState(
    { event, playerEventIsFrom }: ReceivedGameEvent,
    logger: GameEventLogger,
  ): ActionState {
    assert(event.type === "respond_for_block");
    assert(playerEventIsFrom === this.playerTargeted);
    assert(playerEventIsFrom !== this.getCurrentPlayer());
    const { response } = event;
    if (response === "allow") {
      return getFunctionX("STEAL")(this.state, logger);
    }
    const { card } = response;
    assert(card === "CAPTAIN" || card === "AMBASSADOR");
    logger(`${playerEventIsFrom} has tried to block stealing as ${card}`);
    return BlockedStealState.create(
      this.state,
      new Map(),
      playerEventIsFrom,
      card,
    );
  }
}
