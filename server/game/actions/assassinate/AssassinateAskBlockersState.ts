import { ActionState } from "../base/ActionState";
import { PlayerID, ServerGameState } from "../../types/gameTypes";
import { GameEventLogger, ReceivedGameEvent } from "../../types/gameEvents";
import assert from "assert";
import { getFunctionX } from "../influence/InfluenceRevealFunctions";
import { BlockedAssassinateState } from "./BlockedAssassinateState";
import { PlayerAction } from "../../types/PlayerAction";
import { actionState, BaseActionState } from "../base/BaseActionState";
import { jsonMember } from "typedjson";

@actionState
export class AssassinateAskBlockersState extends BaseActionState {
  @jsonMember(String)
  private readonly _playerTargeted?: PlayerID;

  get playerTargeted(): PlayerID {
    assert(this._playerTargeted !== undefined);
    return this._playerTargeted;
  }

  public static create(state: ServerGameState, playerTargeted: PlayerID) {
    return new AssassinateAskBlockersState(state, playerTargeted);
  }

  private constructor(state?: ServerGameState, playerTargeted?: PlayerID) {
    super(state);
    this._playerTargeted = playerTargeted;
  }

  getPlayerActions(): Map<PlayerID, PlayerAction> {
    const playerActions: Map<PlayerID, PlayerAction> = new Map();
    for (const { id } of this.state.gameState.players) {
      if (id === this.playerTargeted) {
        playerActions.set(id, {
          type: "CHOOSE_BLOCK",
          block: {
            possible: ["CONTESSA"],
            playerToBlock: this.getCurrentPlayer(),
          },
        });
      } else {
        playerActions.set(id, { type: "WAIT_FOR_OTHERS" });
      }
    }
    return playerActions;
  }

  nextState(
    { event, playerEventIsFrom }: ReceivedGameEvent,
    logger: GameEventLogger,
  ): ActionState {
    assert(event.type === "respond_for_block");
    assert(playerEventIsFrom === this.playerTargeted);
    assert(playerEventIsFrom !== this.getCurrentPlayer());
    const { response } = event;
    if (response === "allow") {
      return getFunctionX("ASSASSINATE")(this.state, logger);
    }
    const { card } = response;
    assert(card === "CONTESSA");
    logger(
      `${this.playerTargeted} has tried to block assasinate with CONTESSA`,
    );
    return BlockedAssassinateState.create(
      this.state,
      new Map(),
      playerEventIsFrom,
    );
  }
}
