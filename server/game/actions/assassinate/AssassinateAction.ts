import { HiddenCard, PlayerID, ServerGameState } from "../../types/gameTypes";
import { InfluenceRevealFunctionType } from "../influence/InfluenceRevealFunctions";
import { ActionStateWithSingleBlock } from "../base/ActionStateWithSingleBlock";
import { ActionResponse } from "../../types/gameEvents";
import { actionState } from "../base/BaseActionState";

@actionState
export class AssassinateAction extends ActionStateWithSingleBlock {
  readonly correctCard: HiddenCard = "ASSASSIN";
  readonly onSuccessfulClaim: InfluenceRevealFunctionType =
    "ASSASSINATE_ASK_BLOCKER";

  createNew(): ActionStateWithSingleBlock {
    return new AssassinateAction(this.state, this.responses);
  }
  public static create(
    state: ServerGameState,
    responses: Map<PlayerID, ActionResponse>,
  ) {
    return new AssassinateAction(state, responses);
  }
}
