import { PlayerID, ServerGameState } from "../../types/gameTypes";
import { ActionResponse } from "../../types/gameEvents";
import { InfluenceRevealFunctionType } from "../influence/InfluenceRevealFunctions";
import { ActionStateWithSingleBlock } from "../base/ActionStateWithSingleBlock";
import { actionState } from "../base/BaseActionState";

@actionState
export class BlockedAssassinateState extends ActionStateWithSingleBlock {
  readonly correctCard: "CONTESSA" = "CONTESSA";
  readonly onSuccessfulClaim: InfluenceRevealFunctionType = "GO_NEXT_PLAYER";
  readonly onFailedClaim: InfluenceRevealFunctionType = "ASSASSINATE";

  createNew(): ActionStateWithSingleBlock {
    return new BlockedAssassinateState(
      this.state,
      this.responses,
      this.playerThatMadeClaim,
    );
  }

  public static create(
    state: ServerGameState,
    responses: Map<PlayerID, ActionResponse>,
    playerBlockingAssasinate: PlayerID,
  ) {
    return new BlockedAssassinateState(state, responses, playerBlockingAssasinate);
  }
}
