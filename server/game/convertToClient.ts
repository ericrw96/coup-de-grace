import { Card, GameState, HiddenInfluence, PlayerID } from "./types/gameTypes";

export function convertState(
  { players, deck, revealedCards, ...serverState }: GameState<HiddenInfluence>,
  playerID: PlayerID
): GameState<Card> {
  return {
    ...serverState,
    players: players.map((playerInfluences) => {
      return {
        ...playerInfluences,
        influence: playerInfluences.influence.map((influence) =>
          makeCardForPlayer(influence, playerID)
        ),
      };
    }),
    deck: deck.map((influence) => makeCardForPlayer(influence, playerID)),
    revealedCards: revealedCards.map((influence) =>
      makeCardForPlayer(influence, playerID)
    ),
  };
}

export function makeCardForPlayer(
  { visibility, card }: HiddenInfluence,
  playerID: PlayerID
): Card {
  switch (visibility.type) {
    case "visible_to_all":
      return card;
    case "visible_to_player":
      if (playerID === visibility.visibleToPlayerId) {
        return card;
      }
      return "NOT_KNOWN";
    case "not_visible":
      return "NOT_KNOWN";
  }
}
