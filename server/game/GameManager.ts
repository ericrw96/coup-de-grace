import { GameID } from "../SocketTypes/SocketTypes";
import { Game } from "./Game";
import { ClientGameState, PlayerID } from "./types/gameTypes";
import assert from "assert";
import { GameEventLogger, ReceivedGameEvent } from "./types/gameEvents";

import { jsonObject, jsonMapMember, toJson } from "typedjson";

@jsonObject
@toJson
export default class GameManager {
  @jsonMapMember(String, Game)
  private currentGames: Map<GameID, Game> = new Map();

  createNewGame(gameID: GameID, players: Array<PlayerID>): GameID {
    if (this.gameExists(gameID)) {
      return gameID;
    }
    assert(players.length >= 2, "need at least 2 players");
    assert(players.length <= 6, "cannot have more than 6 players");
    const game = Game.createGame(players);
    this.currentGames.set(gameID, game);
    return gameID;
  }

  onEvent(
    gameEvent: ReceivedGameEvent,
    gameID: GameID,
    logger: GameEventLogger,
  ) {
    const currentGame = this.currentGames.get(gameID);
    assert(currentGame !== undefined);
    currentGame.onEvent(gameEvent, logger);
  }

  getGameSocketRoom(gameID: GameID): string {
    return `game-room-${gameID}`;
  }

  gameExists(gameID: GameID): boolean {
    return this.currentGames.get(gameID) !== undefined;
  }

  getPlayerSocket(gameID: GameID, playerID: PlayerID): string {
    return `game-${gameID}-player-${playerID}`;
  }

  getGameState(gameID: GameID, playerID: PlayerID): ClientGameState {
    const currentGame = this.currentGames.get(gameID);
    assert(currentGame !== undefined);
    return currentGame.getGameStateForPlayer(playerID);
  }

  getGameStates(gameID: GameID): Map<PlayerID, ClientGameState> {
    const currentGame = this.currentGames.get(gameID);
    assert(currentGame !== undefined);
    const map = new Map();
    for (const playerID of currentGame.playerIDs) {
      map.set(playerID, currentGame.getGameStateForPlayer(playerID));
    }
    return map;
  }
  getPlayers(gameID: GameID): Readonly<Array<PlayerID>> {
    const currentGame = this.currentGames.get(gameID);
    assert(currentGame !== undefined);
    return currentGame.playerIDs;
  }

  tryRemoveGame(gameID: GameID): boolean {
    return this.currentGames.delete(gameID);
  }
}
