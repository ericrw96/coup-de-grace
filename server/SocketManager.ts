import { Server, Socket } from "socket.io";
import { GameID } from "./SocketTypes/SocketTypes";
import GameManager from "./game/GameManager";
import { PlayerID } from "./game/types/gameTypes";
import assert from "assert";
import { GameEvent, LoggedGameEvent } from "./game/types/gameEvents";
import { PlayerData } from "../src/utils/PlayerData";
import { v4 } from "uuid";
import fs from "fs";
import {
  AnyT,
  ArrayT,
  jsonMapMember,
  jsonMember,
  jsonObject,
  toJson,
  TypedJSON,
} from "typedjson";

@jsonObject
@toJson
export class GameData {
  @jsonMember(GameManager)
  public gameManager: GameManager = new GameManager();
  @jsonMapMember(String, AnyT)
  public clientData = new Map<string, { playerID: PlayerID; gameID: GameID }>();
  @jsonMapMember(String, ArrayT(String))
  public gameEvents = new Map<GameID, Array<LoggedGameEvent>>();
}

export default class SocketManager {
  private io: Server;
  private readonly gameDataSerializer = new TypedJSON(GameData, {
    errorHandler: (error) => {
      throw error;
    },
  });

  private gameData = new GameData();

  private savedStatePath: string;

  constructor(io: Server, savedStatePath: string) {
    this.io = io;
    this.savedStatePath = savedStatePath;
    if (fs.existsSync(savedStatePath)) {
      console.log(`Reading saved state from ${savedStatePath}`);
      console.log(
        this.gameDataSerializer.parse(fs.readFileSync(savedStatePath, "utf8")),
      );
      this.gameData =
        this.gameDataSerializer.parse(
          fs.readFileSync(savedStatePath, "utf8"),
        ) ?? new GameData();
    }
  }
  getPlayerData(socket: Socket): { playerID: PlayerID; gameID: GameID } {
    const data = this.gameData.clientData.get(socket.id);
    assert(data !== undefined);
    return data;
  }

  listen() {
    this.io.on("connection", (socket: Socket) => {
      // lifecycle events
      socket.on("disconnect", () => {
        console.log(`disconnect by ${socket.id}`);
        const data = this.gameData.clientData.get(socket.id);
        if (data !== undefined) {
          socket
            .to(this.gameData.gameManager.getGameSocketRoom(data.gameID))
            .emit("remove_player", data.playerID);
          setTimeout(() => {
            const clientsInRoom = this.io.sockets.adapter.rooms.get(
              this.gameData.gameManager.getGameSocketRoom(data.gameID),
            );
            if ((clientsInRoom?.size ?? 0) === 0) {
              this.gameData.gameManager.tryRemoveGame(data.gameID);
              this.saveGameData();
            }
          }, 300000);
        }
        this.gameData.clientData.delete(socket.id);
      });
      // game events
      socket.on("start_game", async (playerIDs: Array<PlayerID>) => {
        const { gameID } = this.getPlayerData(socket);
        const newGameID = this.gameData.gameManager.createNewGame(
          gameID,
          playerIDs,
        );
        console.log(`created game ${gameID}`);
        this.io
          .to(this.gameData.gameManager.getGameSocketRoom(newGameID))
          .emit("move_to_game", newGameID);
        this.saveGameData();
      });
      socket.on("game_event", async (event: GameEvent) => {
        console.log(`got game event ${JSON.stringify(event)}`);
        const { gameID, playerID } = this.getPlayerData(socket);
        console.log(`game id ${gameID}`);
        this.gameData.gameManager.onEvent(
          {
            event,
            playerEventIsFrom: playerID,
          },
          this.getPlayerData(socket).gameID,
          (message) => {
            const currentEvents = this.gameData.gameEvents.get(gameID) ?? [];
            currentEvents.push(message);
            this.gameData.gameEvents.set(gameID, currentEvents);
            this.io
              .to(this.gameData.gameManager.getGameSocketRoom(gameID))
              .emit("game_event", message);
          },
        );
        this.saveGameData();
        this.broadcastNewState(socket);
      });
      socket.on("get_all_game_events", async () => {
        const { gameID } = this.getPlayerData(socket);
        const currentEvents = this.gameData.gameEvents.get(gameID) ?? [];
        socket.emit("game_events", currentEvents);
      });
      socket.on("back_to_lobby", async () => {
        const newGameID = v4();
        this.io
          .to(
            this.gameData.gameManager.getGameSocketRoom(
              this.getPlayerData(socket).gameID,
            ),
          )
          .emit("move_to_lobby", newGameID);
      });
      // lobby events
      socket.on("update_self", async (playerData: PlayerData) => {
        const { gameID } = this.getPlayerData(socket);
        this.io
          .to(this.gameData.gameManager.getGameSocketRoom(gameID))
          .emit("update_player", playerData);
      });
      // initial connection
      try {
        const { gameID, playerID } = socket.handshake.query;
        console.log(
          `we had a connection by ${socket.id} for gameID ${gameID} and playerID ${playerID}`,
        );
        assert(gameID != null);
        assert(typeof gameID === "string");
        assert(playerID != null);
        assert(typeof playerID === "string");
        socket.join(this.gameData.gameManager.getGameSocketRoom(gameID));
        socket.join(
          this.gameData.gameManager.getPlayerSocket(gameID, playerID),
        );
        this.gameData.clientData.set(socket.id, { playerID, gameID });
        if (this.gameData.gameManager.gameExists(gameID)) {
          this.sendGameStateToPlayer(gameID, playerID);
        }
        socket
          .to(this.gameData.gameManager.getGameSocketRoom(gameID))
          .emit("request_update");
        socket.emit("game_events", this.gameData.gameEvents.get(gameID) ?? []);
      } catch {
        socket.disconnect(true);
      }
    });
  }

  sendGameStateToPlayer(gameID: GameID, playerID: PlayerID): void {
    this.io
      .to(this.gameData.gameManager.getPlayerSocket(gameID, playerID))
      .emit(
        "gameState",
        this.gameData.gameManager.getGameState(gameID, playerID),
      );
  }
  broadcastNewState(socket: Socket): void {
    const { gameID } = this.getPlayerData(socket);
    for (const playerID of this.gameData.gameManager.getPlayers(gameID)) {
      this.sendGameStateToPlayer(gameID, playerID);
    }
  }
  saveGameData(): void {
    fs.writeFileSync(
      this.savedStatePath,
      this.gameDataSerializer.stringify(this.gameData),
    );
  }
}
