import { Server } from "socket.io";
import http from "http";
import SocketManager from "./SocketManager";
import express from "express";
import * as path from "path";
import "reflect-metadata";

const app = express();

const dir = process.env.NODE_ENV === "production" ? "../../build" : "../build";
app.use(express.static(path.resolve(__dirname, dir)));

app.get("*", function (request, response) {
  response.sendFile(path.resolve(__dirname, dir, "index.html"));
});

const httpServer = http.createServer(app);
const io = new Server(httpServer, {
  // cors: {
  //   origin: "*",
  //   methods: ["GET", "POST"],
  // },
});
const port = process.env.PORT || 5000;
export function runServer() {
  new SocketManager(io, "/tmp/gameManagerState.json").listen();
  httpServer.listen(port, () => {
    console.log(`listening for socketio on port ${port}`);
  });
}

runServer();
