import { PlayerID } from "../game/types/gameTypes";

export type GameID = string;

export type JoinGameType = {
  playerID: PlayerID;
  gameID: GameID;
};

export type ClientDataType = { playerID: PlayerID; gameID: GameID };
