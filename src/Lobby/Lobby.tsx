import { GameID } from "../../server/SocketTypes/SocketTypes";
import { Button, List, ListItem, TextField } from "@material-ui/core";
import { usePlayerLobbyState } from "../utils/usePlayerLobbyState";

type Props = {
  gameID: GameID;
};

export default function Lobby({ gameID }: Props) {
  const {
    playerData,
    setPlayerData,
    currentActivePlayers,
    socket,
  } = usePlayerLobbyState(gameID);
  return (
    <>
      <div>
        Hi there{" "}
        <TextField
          value={playerData.playerName}
          onChange={(event) => {
            setPlayerData((playerData) => ({
              ...playerData,
              playerName: event.target.value,
            }));
          }}
        />
      </div>
      <div>This is a lobby for {gameID}</div>
      <div>
        Players
        <List>
          {Object.values(currentActivePlayers).map(
            ({ playerName, playerID }) => (
              <ListItem key={playerID}>
                <div>Player: {playerName}</div>
              </ListItem>
            ),
          )}
        </List>
      </div>
      <div>
        <Button
          variant={"contained"}
          color={"primary"}
          disabled={
            !(
              Object.keys(currentActivePlayers).length > 1 &&
              Object.keys(currentActivePlayers).length < 7
            )
          }
          size={"large"}
          onClick={() =>
            socket.emit("start_game", Object.keys(currentActivePlayers))
          }
        >
          Start Game
        </Button>
      </div>
    </>
  );
}
