import React from "react";
import { Switch, Route, Link } from "react-router-dom";
import Home from "./Home/Home";
import Lobby from "./Lobby/Lobby";
import { Container } from "@material-ui/core";
import Game from "./Game/Game";

export default function App() {
  return (
    <Container>
      <nav>
        <ul>
          {/* add in new routes here*/}
          <li>
            <Link to="/">Home</Link>
          </li>
        </ul>
      </nav>
      <Switch>
        <Route
          path="/lobby/:gameID"
          render={(match) => <Lobby gameID={match.match.params.gameID} />}
        />
        <Route
          path="/game/:gameID"
          render={(match) => <Game gameID={match.match.params.gameID} />}
        />
        <Route path="/">
          <Home />
        </Route>
      </Switch>
    </Container>
  );
}
