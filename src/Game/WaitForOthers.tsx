import { useState } from "react";
import useInterval from "../utils/useInterval";

export function WaitForOthers() {
  const [secondsWaiting, setSecondsWaiting] = useState(0);
  useInterval(() => setSecondsWaiting((s) => s + 1), 1000);
  return (
    <div>
      You are waiting for others to make decisions. Sit back, relax, and watch
      the number of seconds you have waited increase!
      <div>
        You have been waiting for others to decide for {secondsWaiting} seconds.
      </div>
    </div>
  );
}
