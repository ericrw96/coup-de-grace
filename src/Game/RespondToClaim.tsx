import { ActionResponse, GameEvent } from "../../server/game/types/gameEvents";
import { PlayerClaim } from "../../server/game/types/PlayerAction";
import { Button, FormControlLabel, Radio, RadioGroup } from "@material-ui/core";
import { PlayerID } from "../../server/game/types/gameTypes";
import * as React from "react";
import { useState } from "react";
import { CurrentLobbyPlayers } from "../utils/usePlayerLobbyState";

type Props = {
  onGameEvent: (event: GameEvent) => void;
  claim: PlayerClaim;
  currentPlayerID: PlayerID;
  currentLobbyPlayers: CurrentLobbyPlayers;
};

export default function RespondToClaim({
  onGameEvent,
  claim,
  currentPlayerID,
  currentLobbyPlayers,
}: Props) {
  const [response, setResponse] = useState<ActionResponse | undefined>(
    undefined
  );
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setResponse(event.target.value as ActionResponse);
  };
  return (
    <div>
      {currentLobbyPlayers[claim.playerClaiming]?.playerName ??
        claim.playerClaiming}{" "}
      has claimed to be {claim.cardClaimed}.
      <RadioGroup name="How will you respond?" onChange={handleChange}>
        <FormControlLabel
          control={<Radio />}
          label={"Accuse them of lying"}
          value={"claim_lie"}
        />
        <FormControlLabel
          control={<Radio />}
          label={"Allow their action."}
          value={"allow"}
        />
      </RadioGroup>
      <Button
        color="primary"
        onClick={() => {
          if (response === undefined) {
            return;
          }
          onGameEvent({
            type: "respond_to_action",
            player: currentPlayerID,
            response: response,
          });
        }}
        disabled={response === undefined}
      >
        Respond
      </Button>
    </div>
  );
}
