import { Button, Grid, Typography } from "@material-ui/core";
import { useCallback, useMemo, useState } from "react";
import { CurrentLobbyPlayers } from "../utils/usePlayerLobbyState";
import { PlayerID, PlayerState } from "../../server/game/types/gameTypes";
import type { Card as CardType } from "../../server/game/types/gameTypes";
import { PlayerData } from "../utils/PlayerData";
import { PlayerEventAction } from "../../server/game/types/possibleAction";
import { GameEvent } from "../../server/game/types/gameEvents";
import {
  SelectActionDialog,
  SelectedActionDialogValue,
} from "./SelectActionDialog";

type Props = {
  onGameEvent: (event: GameEvent) => void;
  currentLobbyPlayers: CurrentLobbyPlayers;
  alivePlayers: Array<PlayerID>;
  currentPlayer: PlayerState<CardType>;
};

type ActionSelections = {
  header: string;
  requirement: string;
  action: string;
  playerAction: PlayerEventAction;
  targetsOther?: boolean;
  disabled?: boolean;
};

export default function SelectAction({
  onGameEvent,
  currentLobbyPlayers,
  alivePlayers,
  currentPlayer,
}: Props) {
  const [
    selectedAction,
    setSelectedAction,
  ] = useState<SelectedActionDialogValue>();
  const aliveOtherLobbyPlayers = useMemo(
    () =>
      alivePlayers
        .map((playerID) => currentLobbyPlayers[playerID])
        .filter(
          (data): data is PlayerData =>
            data !== undefined && data.playerID !== currentPlayer.id
        ),
    [alivePlayers, currentLobbyPlayers, currentPlayer.id]
  );
  const disabledForCoup = currentPlayer.coins >= 10;
  const needsCoins = (coins: number) => currentPlayer.coins < coins;
  const handleButtonClick = useCallback(
    (
      playerAction: PlayerEventAction,
      targetsOthers: boolean,
      header: string
    ) => {
      if (!targetsOthers) {
        onGameEvent({
          type: "propose_action",
          action: {
            targetPlayerID: currentPlayer.id,
            sourcePlayerID: currentPlayer.id,
            action: playerAction,
          },
        });
      } else {
        setSelectedAction({ header: header, selectedAction: playerAction });
      }
    },
    [currentPlayer.id, onGameEvent]
  );
  const actions: Array<ActionSelections> = [
    {
      header: "Income",
      requirement: "No Requirement",
      action: "Take 1 coin",
      playerAction: "income",
    },
    {
      header: "Foreign Aid",
      requirement: "Blocked by Duke",
      action: "Take 2 coins",
      playerAction: "foreign_aid",
    },
    {
      header: "Coup",
      requirement: "Cannot be Blocked",
      action: "Pay 7 coins, Choose a player to lose influence",
      playerAction: "coup",
      targetsOther: true,
      disabled: needsCoins(7),
    },
    {
      header: "Tax",
      requirement: "Requires Duke",
      action: "Take 3 coins",
      playerAction: "tax",
    },
    {
      header: "Assassinate",
      requirement: "Requires Assassin",
      action: "Pay 3 coins, Choose a player to lose influence",
      playerAction: "assassinate",
      targetsOther: true,
      disabled: needsCoins(3) || disabledForCoup,
    },
    {
      header: "Exchange",
      requirement: "Requires Ambassador",
      action: "Exchange cards with the court deck",
      playerAction: "exchange_with_court_deck",
    },
    {
      header: "Steal",
      requirement: "Requires Captain",
      action: "Steal 2 coins from another player",
      playerAction: "steal",
      targetsOther: true,
    },
  ];
  return (
    <>
      <div>Please Select an action.</div>
      <Grid container spacing={1}>
        {actions.map(
          ({
            header,
            requirement,
            action,
            playerAction,
            targetsOther,
            disabled,
          }) => (
            <Grid item key={header}>
              <Button
                variant="contained"
                onClick={() =>
                  handleButtonClick(playerAction, targetsOther ?? false, header)
                }
                disabled={disabled ?? disabledForCoup}
              >
                <div>
                  <Typography variant={"h4"}>{header}</Typography>
                  <Typography>{requirement}</Typography>
                  <Typography>{action}</Typography>
                </div>
              </Button>
            </Grid>
          )
        )}
      </Grid>
      <SelectActionDialog
        selectedAction={selectedAction}
        aliveCurrentPlayers={aliveOtherLobbyPlayers}
        onSelectedOtherPlayer={(playerID) => {
          if (selectedAction === undefined) {
            return;
          }
          if (playerID !== undefined) {
            onGameEvent({
              type: "propose_action",
              action: {
                targetPlayerID: playerID,
                sourcePlayerID: currentPlayer.id,
                action: selectedAction.selectedAction,
              },
            });
          }
          setSelectedAction(undefined);
        }}
      />
    </>
  );
}
