import type { Card as CardType } from "../../server/game/types/gameTypes";
import {
  ClientGameState,
  PlayerState,
} from "../../server/game/types/gameTypes";
import { usePlayerID } from "../utils/PlayerData";
import { Card, Grid, List, ListItem, Tooltip } from "@material-ui/core";
import { CurrentLobbyPlayers } from "../utils/usePlayerLobbyState";
import { WaitForOthers } from "./WaitForOthers";
import SelectAction from "./SelectAction";
import { Socket } from "socket.io-client";
import { useCallback } from "react";
import { GameEvent } from "../../server/game/types/gameEvents";
import RevealInfluence from "./RevealInfluence";
import RespondToClaim from "./RespondToClaim";
import { SelectInfluence } from "./SelectInfluence";
import { ChooseBlock } from "./ChooseBlock";
import { GameOver } from "./GameOver";

export function displayAction(
  gameState: ClientGameState,
  currentLobbyPlayers: CurrentLobbyPlayers,
  currentPlayer: PlayerState<CardType>,
  onGameEvent: (event: GameEvent) => void,
  onNewGame: () => void,
) {
  const { playerAction } = gameState;
  switch (playerAction.type) {
    case "GAME_OVER":
      return (
        <GameOver
          currentLobbyPlayers={currentLobbyPlayers}
          currentPlayerID={currentPlayer.id}
          currentPlayers={gameState.gameState.players}
          onNewGame={onNewGame}
        />
      );
    case "RESPOND_TO_CLAIM":
      return (
        <RespondToClaim
          claim={playerAction.claim}
          onGameEvent={onGameEvent}
          currentPlayerID={currentPlayer.id}
          currentLobbyPlayers={currentLobbyPlayers}
        />
      );
    case "WAIT_FOR_OTHERS":
      return <WaitForOthers />;
    case "SELECT_ACTION":
      const alivePlayers = gameState.gameState.players
        .filter((player) => player.influence.length > 0)
        .map((player) => player.id);
      return (
        <SelectAction
          alivePlayers={alivePlayers}
          currentLobbyPlayers={currentLobbyPlayers}
          currentPlayer={currentPlayer}
          onGameEvent={onGameEvent}
        />
      );
    case "REVEAL_INFLUENCE":
      return (
        <RevealInfluence
          currentPlayer={currentPlayer}
          onGameEvent={onGameEvent}
        />
      );
    case "BLOCK_FOREIGN_AID":
      return (
        <ChooseBlock
          block={playerAction.block}
          currentLobbyPlayers={currentLobbyPlayers}
          onGameEvent={onGameEvent}
          isForeignAid
        />
      );
    case "SELECT_INFLUENCE":
      return (
        <SelectInfluence
          currentPlayer={currentPlayer}
          onGameEvent={onGameEvent}
          additionalInfluence={playerAction.additionalInfluence}
        />
      );
    case "CHOOSE_BLOCK":
      return (
        <ChooseBlock
          block={playerAction.block}
          onGameEvent={onGameEvent}
          currentLobbyPlayers={currentLobbyPlayers}
        />
      );
  }
}
type Props = {
  socket: Socket;
  gameState: ClientGameState;
  currentPlayers: CurrentLobbyPlayers;
};
export function GameView({ gameState, currentPlayers, socket }: Props) {
  const [myID] = usePlayerID();
  const currentPlayer = gameState.gameState.players.find(
    (player) => player.id === myID,
  );
  const onGameEvent = useCallback(
    (event) => {
      socket.emit("game_event", event);
    },
    [socket],
  );
  const onNewGame = useCallback(() => {
    socket.emit("back_to_lobby");
  }, [socket]);
  if (currentPlayer === undefined) {
    return (
      <>
        <div>You were not found, you must be spectating.</div>
      </>
    );
  }
  return (
    <>
      <Card>
        <List>
          {gameState.gameState.players.map(({ id, influence, coins }) => {
            const isMyPlayer = id === myID;
            const isCurrentPlayer = id === gameState.gameState.currentPlayerId;
            const [color, tooltip] =
              currentPlayers[id]?.active === true
                ? ["green", "Player is active"]
                : ["red", "Player is inactive"];
            return (
              <ListItem key={id}>
                <Grid container spacing={1}>
                  <Grid item style={{ display: "flex" }}>
                    {currentPlayers[id]?.playerName}
                  </Grid>
                  <Grid item style={{ display: "flex" }}>
                    {JSON.stringify(influence)}
                  </Grid>
                  <Grid item style={{ display: "flex" }}>
                    Coins: {coins}
                  </Grid>
                  <Grid item style={{ display: "flex" }} color={color}>
                    <Tooltip title={tooltip} arrow>
                      <span>{"\u25CF"}</span>
                    </Tooltip>
                  </Grid>
                  {isMyPlayer ? (
                    <Grid item style={{ display: "flex" }}>
                      <b>{"<-"} you</b>
                    </Grid>
                  ) : null}
                  {isCurrentPlayer ? (
                    <Grid item style={{ display: "flex" }}>
                      <b>{"<-"} current player</b>
                    </Grid>
                  ) : null}
                </Grid>
              </ListItem>
            );
          })}
        </List>
        <div>Your Action: {gameState.playerAction.type}</div>
      </Card>
      <Card variant="outlined">
        {displayAction(
          gameState,
          currentPlayers,
          currentPlayer,
          onGameEvent,
          onNewGame,
        )}
      </Card>
    </>
  );
}
