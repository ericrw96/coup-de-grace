import { GameEvent } from "../../server/game/types/gameEvents";
import {
  Card as CardType,
  HiddenCard,
  PlayerState,
} from "../../server/game/types/gameTypes";
import { Button, FormControlLabel, Radio, RadioGroup } from "@material-ui/core";
import * as React from "react";
import { useState } from "react";

type Props = {
  onGameEvent: (event: GameEvent) => void;
  currentPlayer: PlayerState<CardType>;
};

export default function RevealInfluence({ onGameEvent, currentPlayer }: Props) {
  const [selectedIdx, setSelectedIdx] = useState<number | undefined>();
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSelectedIdx(Number.parseInt(event.target.value));
  };
  return (
    <>
      <div>You must reveal your influence</div>
      <RadioGroup name="Selected Influence" onChange={handleChange}>
        {currentPlayer.influence.map((card, index) => (
          <FormControlLabel
            value={`${index}`}
            key={`${index}`}
            control={<Radio />}
            label={card}
          />
        ))}
      </RadioGroup>
      <Button
        color="primary"
        onClick={() => {
          if (selectedIdx === undefined) {
            return;
          }
          const card = currentPlayer.influence[selectedIdx];
          if (card === undefined) {
            return;
          }
          onGameEvent({
            type: "reveal_influence",
            cardToReveal: card as HiddenCard,
            player: currentPlayer.id,
          });
        }}
        disabled={selectedIdx === undefined}
      >
        Reveal{" "}
        {selectedIdx !== undefined ? currentPlayer.influence[selectedIdx] : ""}
      </Button>
    </>
  );
}
