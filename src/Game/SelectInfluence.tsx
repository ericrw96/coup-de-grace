import { GameEvent } from "../../server/game/types/gameEvents";
import {
  Card as CardType,
  HiddenCard,
  PlayerState,
} from "../../server/game/types/gameTypes";
import * as React from "react";
import { AdditionalInfluence } from "../../server/game/types/PlayerAction";
import {
  Button,
  Checkbox,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormHelperText,
  Typography,
} from "@material-ui/core";

type Props = {
  onGameEvent: (event: GameEvent) => void;
  currentPlayer: PlayerState<CardType>;
  additionalInfluence: AdditionalInfluence;
};

export function SelectInfluence({
  onGameEvent,
  currentPlayer,
  additionalInfluence,
}: Props) {
  const numberToSelect = currentPlayer.influence.length;
  const allCards = currentPlayer.influence.concat(additionalInfluence.cards);

  const [checkedCards, setCheckedCards] = React.useState<Array<boolean>>(
    allCards.map(() => false)
  );

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setCheckedCards((checkedCards) =>
      checkedCards.map((v, idx) =>
        idx === Number.parseInt(event.target.name) ? event.target.checked : v
      )
    );
  };
  const error = checkedCards.filter((v) => v).length !== numberToSelect;
  return (
    <div>
      <FormControl required error={error} component="fieldset">
        <FormHelperText>
          <Typography variant={"h5"} component={"span"}>
            Select {numberToSelect} influence
          </Typography>
        </FormHelperText>
        <FormGroup>
          {checkedCards.map((checked, idx) => {
            return (
              <FormControlLabel
                control={
                  <Checkbox
                    checked={checked}
                    onChange={handleChange}
                    name={`${idx}`}
                  />
                }
                label={allCards[idx]}
                key={idx}
              />
            );
          })}
        </FormGroup>
      </FormControl>
      <div>
        <Button
          color="primary"
          variant="contained"
          onClick={() => {
            if (error) {
              return;
            }
            onGameEvent({
              type: "select_influence",
              influence_selected: allCards
                .filter((_, idx) => checkedCards[idx])
                .filter((card): card is HiddenCard => true),
            });
          }}
          disabled={error}
        >
          Select {allCards.filter((_, idx) => checkedCards[idx]).join(" and ")}
        </Button>
      </div>
    </div>
  );
}
