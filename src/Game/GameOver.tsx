import { CurrentLobbyPlayers } from "../utils/usePlayerLobbyState";
import {
  Card as CardType,
  PlayerID,
  PlayerState,
} from "../../server/game/types/gameTypes";
import { Button, Typography } from "@material-ui/core";
import * as React from "react";

type Props = {
  currentLobbyPlayers: CurrentLobbyPlayers;
  currentPlayers: Array<PlayerState<CardType>>;
  currentPlayerID: PlayerID;
  onNewGame: () => void;
};

export function GameOver({
  currentLobbyPlayers,
  currentPlayers,
  currentPlayerID,
  onNewGame,
}: Props) {
  const winningPlayer = currentPlayers.find(
    (player) => player.influence.length > 0
  );
  let notice;
  if (winningPlayer === undefined) {
    notice = <div>Hmm that is weird, no one won but the game is over.</div>;
  } else if (currentPlayerID === winningPlayer.id) {
    notice = <div>You won!</div>;
  } else {
    const player = currentLobbyPlayers[winningPlayer.id];
    notice = player !== undefined ? <div>{player.playerName} Wins.</div> : null;
  }
  return (
    <>
      <div>
        <Typography variant={"h5"} component={"span"}>
          Game Over!
        </Typography>
        {notice}
        <Button color="primary" variant="contained" onClick={onNewGame}>
          New Game?
        </Button>
      </div>
    </>
  );
}
