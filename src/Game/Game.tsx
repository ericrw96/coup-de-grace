import { Grid, Typography, Box } from "@material-ui/core";
import { GameID } from "../../server/SocketTypes/SocketTypes";
import { usePlayerGameState } from "../utils/usePlayerGameState";
import { GameView } from "./GameView";
import AutoSizer from "react-virtualized-auto-sizer";
import { FixedSizeList, ListChildComponentProps } from "react-window";
import { CurrentLobbyPlayers } from "../utils/usePlayerLobbyState";

type Props = {
  gameID: GameID;
};

function substitutePlayerNames(
  event: string | undefined,
  currentLobbyPlayers: CurrentLobbyPlayers,
): string {
  let subString = event ?? "";
  for (const [playerID, playerData] of Object.entries(currentLobbyPlayers)) {
    subString = subString.replaceAll(playerID, playerData.playerName);
  }
  return subString;
}

export default function Game({ gameID }: Props) {
  const {
    socket,
    gameState,
    currentLobbyPlayers,
    gameEvents,
  } = usePlayerGameState(gameID);
  if (gameState === undefined) {
    return <div>Loading the game :) {gameID}</div>;
  }
  const usedGameEvents = [...gameEvents].reverse();
  const GameEventListItem = ({ index, style }: ListChildComponentProps) => (
    <div key={index} style={style}>
      {substitutePlayerNames(usedGameEvents[index], currentLobbyPlayers)}
    </div>
  );
  return (
    <Grid container direction="column" spacing={1}>
      <Grid item>
        <GameView
          gameState={gameState}
          currentPlayers={currentLobbyPlayers}
          socket={socket}
        />
      </Grid>
      <Grid item>
        <Box height="50vh">
          <Typography variant="h5">Game Events</Typography>
          <AutoSizer>
            {({ height, width }) => (
              <FixedSizeList
                height={height}
                itemCount={usedGameEvents.length}
                itemSize={35}
                width={width}
              >
                {GameEventListItem}
              </FixedSizeList>
            )}
          </AutoSizer>
        </Box>
      </Grid>
    </Grid>
  );
}
