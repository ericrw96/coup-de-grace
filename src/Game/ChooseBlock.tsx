import { Block } from "../../server/game/types/PlayerAction";
import { GameEvent } from "../../server/game/types/gameEvents";
import { useState } from "react";
import * as React from "react";
import { Button, FormControlLabel, Radio, RadioGroup } from "@material-ui/core";
import { HiddenCard } from "../../server/game/types/gameTypes";
import { CurrentLobbyPlayers } from "../utils/usePlayerLobbyState";

type Props = {
  block: Block;
  currentLobbyPlayers: CurrentLobbyPlayers;
  onGameEvent: (event: GameEvent) => void;
  isForeignAid?: boolean;
};
export function ChooseBlock({
  block,
  onGameEvent,
  currentLobbyPlayers,
  isForeignAid,
}: Props) {
  const [response, setResponse] = useState<HiddenCard | "allow" | undefined>(
    undefined
  );
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.value === "allow") {
      setResponse("allow");
    } else {
      setResponse(event.target.value as HiddenCard);
    }
  };
  return (
    <>
      <div>
        {isForeignAid
          ? `${
              currentLobbyPlayers[block.playerToBlock]?.playerName ??
              block.playerToBlock
            } would like to take foreign aid`
          : `You are being targeted by ${
              currentLobbyPlayers[block.playerToBlock]?.playerName ??
              block.playerToBlock
            }`}
      </div>
      <RadioGroup name="How will you respond?" onChange={handleChange}>
        {block.possible.map((card) => (
          <FormControlLabel
            control={<Radio />}
            label={`Block with ${card}`}
            value={card}
            key={card}
          />
        ))}
        <FormControlLabel
          control={<Radio />}
          label={"Do not block"}
          value={"allow"}
        />
      </RadioGroup>
      <Button
        color="primary"
        onClick={() => {
          if (response === undefined) {
            return;
          }
          if (response === "allow") {
            return onGameEvent({
              type: "respond_for_block",
              response: "allow",
            });
          }
          return onGameEvent({
            type: "respond_for_block",
            response: {
              type: "block",
              card: response,
            },
          });
        }}
        disabled={response === undefined}
      >
        Respond
      </Button>
    </>
  );
}
