import { PlayerData } from "../utils/PlayerData";
import { PlayerID } from "../../server/game/types/gameTypes";
import { useState } from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControlLabel,
  Radio,
  RadioGroup,
} from "@material-ui/core";
import * as React from "react";
import { PlayerEventAction } from "../../server/game/types/possibleAction";

export type SelectedActionDialogValue =
  | {
      header: string;
      selectedAction: PlayerEventAction;
    }
  | undefined;

type Props = {
  selectedAction: SelectedActionDialogValue;
  onSelectedOtherPlayer: (playerID: PlayerID | undefined) => void;
  aliveCurrentPlayers: Array<PlayerData>;
};

export function SelectActionDialog({
  selectedAction,
  onSelectedOtherPlayer,
  aliveCurrentPlayers,
}: Props) {
  const [playerID, setPlayerID] = useState<PlayerID | undefined>(undefined);
  const handleCancel = () => {
    onSelectedOtherPlayer(undefined);
  };

  const handleOk = () => {
    onSelectedOtherPlayer(playerID);
  };

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPlayerID(event.target.value);
  };

  return selectedAction !== undefined ? (
    <Dialog maxWidth="xs" open={true} onClose={handleCancel}>
      <DialogTitle>
        Select player to target with {selectedAction.header}
      </DialogTitle>
      <DialogContent dividers>
        <RadioGroup name="Target Player" onChange={handleChange}>
          {aliveCurrentPlayers.map((option) => (
            <FormControlLabel
              value={option.playerID}
              key={option.playerID}
              control={<Radio />}
              label={option.playerName}
            />
          ))}
        </RadioGroup>
      </DialogContent>
      <DialogActions>
        <Button autoFocus onClick={handleCancel} color="primary">
          Cancel
        </Button>
        <Button
          onClick={handleOk}
          color="primary"
          disabled={playerID === undefined}
        >
          Ok
        </Button>
      </DialogActions>
    </Dialog>
  ) : null;
}
