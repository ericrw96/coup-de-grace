import { useEffect, useState } from "react";
import { io, Socket, SocketOptions } from "socket.io-client";
import { ManagerOptions } from "socket.io-client/build/manager";

export const useSocket = (
  url?: string,
  opts: Partial<ManagerOptions & SocketOptions> | undefined = undefined
) => {
  const socketOpts = { ...opts, autoConnect: false };
  const produceSocket = () =>
    url !== undefined ? io(url, socketOpts) : io(socketOpts);
  const [socket] = useState<Socket>(produceSocket);
  useEffect(() => {
    socket.connect();
    return () => {
      socket.close();
    };
  }, [socket]);
  return socket;
};
