import { PlayerID } from "../../server/game/types/gameTypes";
import { v4 } from "uuid";
import {
  adjectives,
  animals,
  colors,
  uniqueNamesGenerator,
} from "unique-names-generator";
import { createLocalStorageStateHook } from "use-local-storage-state";
import { useEffect } from "react";

export type PlayerData = {
  playerID: PlayerID;
  playerName: string;
  active?: boolean;
};

function generatePlayerData(playerData: PlayerData | undefined): PlayerData {
  if (playerData !== undefined) {
    return playerData;
  }
  return {
    playerID: v4(),
    playerName: uniqueNamesGenerator({
      dictionaries: [adjectives, colors, animals],
    }),
  };
}

const usePlayerData = createLocalStorageStateHook<PlayerData>(
  "playerState",
  () => generatePlayerData(undefined),
);

export default function usePlayerState(): ReturnType<typeof usePlayerData> {
  const [playerState, setPlayerState, isPersisted] = usePlayerData();
  useEffect(() => {
    setPlayerState(playerState);
  }, [playerState, setPlayerState]);
  return [playerState, setPlayerState, isPersisted];
}

export function usePlayerName(): [
  PlayerData["playerName"],
  (playerName: PlayerData["playerName"]) => void,
] {
  const [playerState, setPlayerState] = usePlayerState();
  return [
    playerState.playerName,
    (playerName: PlayerData["playerName"]) =>
      setPlayerState((playerData: PlayerData) => ({
        ...playerData,
        playerName,
      })),
  ];
}

export function usePlayerID(): [
  PlayerData["playerID"],
  (playerID: PlayerData["playerID"]) => void,
] {
  const [playerState, setPlayerState] = usePlayerState();
  return [
    playerState.playerID,
    (playerID: PlayerData["playerID"]) =>
      setPlayerState((playerData: PlayerData) => ({
        ...playerData,
        playerID,
      })),
  ];
}
