import { GameID } from "../../server/SocketTypes/SocketTypes";
import { usePlayerLobbyState } from "./usePlayerLobbyState";
import { useEffect, useState } from "react";
import { ClientGameState } from "../../server/game/types/gameTypes";
import { useHistory } from "react-router-dom";
import { LoggedGameEvent } from "../../server/game/types/gameEvents";

export function usePlayerGameState(gameID: GameID) {
  const {
    playerData,
    currentPlayers: currentLobbyPlayers,
    socket,
    setPlayerData,
    currentActivePlayers,
  } = usePlayerLobbyState(gameID);
  const history = useHistory();
  const [gameState, setGameState] = useState<ClientGameState>();
  const [gameEvents, setGameEvents] = useState<Array<LoggedGameEvent>>([]);
  useEffect(() => {
    const updateGameState = (gameState: ClientGameState) => {
      setGameState(gameState);
    };
    const moveToLobby = (gameID: GameID) => {
      history.push(`/lobby/${gameID}`);
    };
    const updateGameEvents = (events: Array<LoggedGameEvent>) => {
      setGameEvents(events);
    };
    const addGameEvent = (event: LoggedGameEvent) => {
      setGameEvents((events) => [...events, event]);
    };
    socket.on("gameState", updateGameState);
    socket.on("move_to_lobby", moveToLobby);
    socket.on("game_events", updateGameEvents);
    socket.on("game_event", addGameEvent);
    return () => {
      socket.off("gameState", updateGameState);
      socket.off("move_to_lobby", moveToLobby);
      socket.off("game_events", updateGameEvents);
      socket.off("game_event", addGameEvent);
    };
  });
  return {
    playerData,
    currentLobbyPlayers,
    currentActivePlayers,
    socket,
    setPlayerData,
    gameState,
    gameEvents,
  };
}
