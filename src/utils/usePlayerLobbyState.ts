import { useHistory } from "react-router-dom";
import usePlayerState, { PlayerData } from "./PlayerData";
import { useEffect, useMemo, useState } from "react";
import { useSocket } from "./useSocket";
import { PlayerID } from "../../server/game/types/gameTypes";
import { GameID } from "../../server/SocketTypes/SocketTypes";

export type CurrentLobbyPlayers = {
  [key: string]: PlayerData;
};
export function usePlayerLobbyState(gameID: string) {
  const history = useHistory();
  const [playerData, setPlayerData] = usePlayerState();
  const [currentPlayers, updatePlayers] = useState<CurrentLobbyPlayers>({
    [playerData.playerID]: { ...playerData, active: true },
  });
  const socket = useSocket(undefined, {
    query: {
      gameID: `${gameID}`,
      playerID: `${playerData.playerID}`,
    },
  });
  useEffect(() => {
    const updateListener = (playerData: PlayerData) => {
      return updatePlayers((currentPlayers) => ({
        ...currentPlayers,
        [playerData.playerID]: { ...playerData, active: true },
      }));
    };
    const removeListener = (playerID: PlayerID) => {
      return updatePlayers((currentPlayers) => {
        const { [playerID]: player, ...playersWithoutPlayer } = currentPlayers;
        if (player !== undefined) {
          player.active = false;
          return { ...playersWithoutPlayer, [playerID]: player };
        }
        return playersWithoutPlayer;
      });
    };
    const moveToGame = (gameID: GameID) => {
      return history.push(`/game/${gameID}`);
    };
    socket.on("update_player", updateListener);
    socket.on("remove_player", removeListener);
    socket.on("move_to_game", moveToGame);
    return () => {
      socket.off("update_player", updateListener);
      socket.off("remove_player", removeListener);
      socket.off("move_to_game", moveToGame);
    };
  }, [history, socket]);
  useEffect(() => {
    const updateOthers = () => {
      socket.emit("update_self", playerData);
    };
    socket.on("request_update", updateOthers);
    // optimistically update self
    updatePlayers((currentPlayers) => ({
      ...currentPlayers,
      [playerData.playerID]: { ...playerData, active: true },
    }));
    updateOthers();
    return () => {
      socket.off("request_update", updateOthers);
    };
  }, [socket, playerData]);
  const currentActivePlayers = useMemo(() => {
    let newObj: CurrentLobbyPlayers = {};
    for (const [key, value] of Object.entries(currentPlayers)) {
      if (value.active === true) {
        newObj = { ...newObj, [key]: value };
      }
    }
    return newObj;
  }, [currentPlayers]);
  return {
    playerData,
    setPlayerData,
    currentPlayers,
    socket,
    currentActivePlayers,
  };
}
