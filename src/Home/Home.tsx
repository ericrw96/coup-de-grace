import { Button, Grid } from "@material-ui/core";
import { Link } from "react-router-dom";
import { v4 } from "uuid";
import JoinLobby from "./JoinLobby";

export default function Home() {
  const gameID = v4();
  return (
    <Grid container spacing={1}>
      <Grid item xs={12}>
        Online Coup
      </Grid>
      <Grid item xs={12}>
        <JoinLobby />
      </Grid>
      <Grid item xs={12}>
        <Button
          variant="contained"
          color={"primary"}
          component={Link}
          to={`lobby/${gameID}`}
          size={"large"}
        >
          Create Game
        </Button>
      </Grid>
    </Grid>
  );
}
