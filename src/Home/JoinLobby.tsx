import { Button, Grid, TextField } from "@material-ui/core";
import { Link } from "react-router-dom";
import { useState } from "react";
import { validate } from "uuid";

export default function JoinLobby() {
  const [gameID, setGameID] = useState<string | undefined>();
  const validateGameID = (gameID: string | undefined) =>
    gameID === undefined || !validate(gameID);
  return (
    <>
      <Grid container spacing={1}>
        <Grid item style={{ display: "flex" }}>
          <Button
            variant={"contained"}
            color={"primary"}
            component={Link}
            to={`lobby/${gameID}`}
            disabled={validateGameID(gameID)}
            size={"large"}
          >
            Join Game
          </Button>
        </Grid>
        <Grid item>
          <TextField
            label="Game ID"
            variant="outlined"
            error={validateGameID(gameID)}
            value={gameID}
            onChange={(event) => setGameID(event.target.value)}
          />
        </Grid>
      </Grid>
    </>
  );
}
