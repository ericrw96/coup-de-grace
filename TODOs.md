# The set of TODOS to get the project off the ground


Need to store local state for new default.



## server
- spec out the entrypoint to the game actions
    - keep the game state on a per room basis (if we go with socketio)
    - parse client events and pass on as events to the current action
    - send player actions back down to the clients
    
- tests
  - a testing framework to easily mockup an initial game state (without needing to do it all)
  - unit test each InfluenceRevealFunction 

## client
- needs everything
    - landing page
    - creating/joining a room
    - the game page itself
      - take the current state action and display something for it
      - buttons to send back actions to the server
